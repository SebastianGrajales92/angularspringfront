import { Component, OnInit } from '@angular/core';
import { Usuario } from './usuario';
import swal from 'sweetalert2';
import {Router} from '@angular/router'
import { AuthService } from './auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  titulo: string = 'Por favor Sign In!';
  usuario : Usuario;

  constructor(private router:Router, private authService:AuthService) {
    this.usuario = new Usuario();
   }

  ngOnInit(): void {
    if(this.authService.isAuthenticated()){
      swal.fire('Login', `Hola ${this.authService.usuario.username} ya estas autenticado`, 'info');
      this.router.navigate(['/clientes']);
    }
  }

  login(){
    console.log(this.usuario);
    if(this.usuario.username == null || this.usuario.password == null){
      swal.fire('Error login','Username o password vacias!','error')
      return;
    }

    this.authService.login(this.usuario).subscribe(response=>{
      console.log(response);
      this.authService.guardarUsuario(response.access_token);
      this.authService.guardarToken(response.access_token);
      this.router.navigate(['/clientes']);
      let usuario = this.authService.usuario;
      swal.fire('Login',`Hola ${usuario.username}, has iniciado sesion con exito!`, 'success' );
    },err=>{
      if(err.status == 400){
        swal.fire('Error Login','usuario o contraseña invalida', 'error');
      }
    });
  }
}
